export const GAPIKEY = "AIzaSyBDB5_0TZDSCVtC8I22Ra4jly3jhDW2WvY";
import "whatwg-fetch";
import actionsServer from "./actionsServer";

function checkLatLng(lat, lng) {
  if (lat > 50 && lat < 60 && lng > 30 && lng < 40) return true;
  return false;
}

export default {
  ...actionsServer,
  sendMail(context) {
    let htmlPart = `
        <!doctype html>
        <html>
        <body>
        <div style="padding: 7px;">
        <b>Контакт</b><br>
        Имя: ${context.state.fio}<br>
        Телефон: ${context.state.phone}<br>        
        <br>`;

    if (context.state.callRequire) {
      htmlPart =
        htmlPart +
        `
            Требуется звонок оператора!
            <br><br>
            `;
    }

    if (context.state.selectedPayParam == "cash") {
      htmlPart =
        htmlPart +
        `
            <b>Оплата наличными</b><br>
            Сдача с: ${context.state.chargeFor}<br><br>
            `;
    } else {
      htmlPart =
        htmlPart +
        `            
            <b>Оплата картой</b><br><br>
            `;
    }

    if (context.state.deliveryType == 0) {
      htmlPart =
        htmlPart +
        `
            <b>Доставка</b><br>
            Адрес: ${context.state.street} ${context.state.building}, подъезд ${
          context.state.entrance
        }, этаж ${context.state.floor}, кв/офис ${context.state.room}<br>
            `;
    } else if (context.state.deliveryType == 1) {
      htmlPart =
        htmlPart +
        `
            <b>Доставка ко времени</b><br>
            Адрес: ${context.state.street} ${context.state.building}, подъезд ${
          context.state.entrance
        }, этаж ${context.state.floor}, кв/офис ${context.state.room}<br>
            Время: ${context.state.time}<br>
            `;
    } else {
      htmlPart =
        htmlPart +
        `            
            <b>Заберет сам ко времени</b><br>
            Время: ${context.state.time}<br>
            `;
    }

    htmlPart =
      htmlPart +
      `
        <br>
        Сумма: ${context.getters.getOrderSum}<br>        
        Комментарий: ${context.state.comment}<br>
        <br>
        Обработчик: ${
          context.state.deliveryType == 2
            ? context.getters.getSelectedPolygon.text
            : context.state.polygonText
        }<br>
        <br>
        <br>
        <b>Позиции</b><br>
        </div>
        <table cellspacing="0' cellpadding="0">        
        `;
    const aggregated = [...context.state.aggregated.values()];
    aggregated.forEach(item => {
      htmlPart =
        htmlPart +
        `
            <tr>                
                <td style="text-align: left; width:300px;padding: 7px;border-bottom: 1px solid #DDDD;">
                    <div>${item.name.toUpperCase()}</div>                                            
                    <div>
                        <div>Размер: ${item.sizeText}</div>
                        <div>Тесто: ${
                          item.thicknessText
                        }</div>                                
                        <div>Соус: ${
                          item.sauceText
                        }</div>                                
                    </div>                   
                </td>
                <td style="text-align:center;width:150px;padding: 7px;border-bottom: 1px solid #DDDD;">                    
                    ${item.quantity}
                </td>
                <td style="text-align:right;width:150px;padding: 7px;border-bottom: 1px solid #DDDD;">                    
                    ${item.orderPrice * item.quantity} ₽
                </td>                
            </tr>    
            `;
    });

    htmlPart =
      htmlPart +
      `
      </table>
      <br>
      <br>
      <div style="padding: 7px;">
      <b>UTM-метки</b><br>
      Источник: ${context.state.utm_source}<br>
      Тип трафика: ${context.state.utm_medium}<br>
      Название кампании: ${context.state.utm_campaign}<br>
      Запрос: ${context.state.utm_term}<br>
      </div>
      </body>
      </html>
    `;

    const mess = {
      message: {
        Messages: [
          {
            From: {
              Email: "site.mappiz@gmail.com",
              Name: "Мапицца"
            },
            To: [
              {
                Email:
                  context.state.deliveryType == 2
                    ? context.getters.getSelectedPolygon.mail
                    : context.state.polygonMail
                // Email: "denis.gutarin@gmail.com"
              },
              {
                Email: "stat@propremuim.ru"
                // Email: "dgutarin@sdvor.com"
              }
            ],
            Subject: `Заказ мапицца ${Date.now().toString()}`,
            HtmlPart: htmlPart
          }
        ]
      }
    };

    fetch(
      "https://us-central1-starry-battery-207307.cloudfunctions.net/sendPizzaMail",
      {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: JSON.stringify(mess),
        timeout: 5000
      }
    );

    context.commit("setOrderSent");
  },

  sendFranchiseRequest(context) {
    let htmlPart = `
        <!doctype html>
        <html>
        <body>
        <div style="padding: 7px;">
        <b>Запрос на франшизу</b><br>
        Имя: ${context.state.fio}<br>
        Телефон: ${context.state.phone}<br>        
        E-mail: ${context.state.email}<br>        
        <br>
        </div>
        </body>
        </html>
        `;

    const mess = {
      message: {
        Messages: [
          {
            From: {
              Email: "site.mappiz@gmail.com",
              Name: "Мапицца"
            },
            To: [
              {
                Email: "mapizza@bk.ru"
                // Email: "denis.gutarin@gmail.com"
              }
            ],
            Subject: `Запрос на франшизу`,
            HtmlPart: htmlPart
          }
        ]
      }
    };

    fetch(
      "https://us-central1-starry-battery-207307.cloudfunctions.net/sendPizzaMail",
      {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: JSON.stringify(mess),
        timeout: 5000
      }
    );

    context.commit("setOrderSent");
  },

  fetchLatLng(context) {
    return new Promise((resolve, reject) => {
      const api = `https://maps.googleapis.com/maps/api/geocode/json?address=Москва+${
        context.state.street
      }+${context.state.building}&components=country:RU&key=${GAPIKEY}`;
      fetch(api).then(response => {
        response.json().then(data => {
          const result = data.results[0];
          const location = result.geometry.location;
          if (location) {
            if (checkLatLng(location.lat, location.lng)) {
              context.commit("updateLatLng", {
                latitude: location.lat,
                longitude: location.lng
              });
              context.commit("checkDelivery", {
                latitude: location.lat,
                longitude: location.lng
              });
            }
          }
          resolve();
        });
      });
    });
  }
};
