import axios from "axios";

const url = "https://ancient-basin-32157.herokuapp.com/orders";

export default {
  sendOrder(context) {
    return new Promise((resolve, reject) => {
      const body = {
        fio: context.state.fio || "",
        phone: context.state.phone || "",
        callRequire: context.state.callRequire || false,
        selectedPayParam: context.state.selectedPayParam || "cash",
        changeFor: context.state.chargeFor || "",
        street: context.state.street || "",
        building: context.state.building || "",
        entrance: context.state.entrance || "",
        floor: context.state.floor || "",
        room: context.state.room || "",
        time: context.state.time || "",
        totalSum: context.getters.getOrderSum || 0,
        comment: context.state.comment || "",
        deliveryType: context.state.deliveryType || 0,
        store:
          context.state.deliveryType == 2
            ? context.getters.getSelectedPolygon.text || ""
            : context.state.polygonText || "",
        storeMail:
          context.state.deliveryType == 2
            ? context.getters.getSelectedPolygon.mail || ""
            : context.state.polygonMail || "",
        items: [...context.state.aggregated.values()].map(item => ({
          name: item.name.toUpperCase() || "",
          size: item.sizeText || "",
          thickness: item.thicknessText || "",
          sauce: item.sauceText || "",
          quantity: item.quantity || 0,
          orderPrice: item.orderPrice || 0,
          sum: item.orderPrice * item.quantity || 0
        })),
        utmSource: context.state.utm_source || "",
        utmMedium: context.state.utm_medium || "",
        utmCampaign: context.state.utm_campaign || "",
        utmTerm: context.state.utm_term || ""
      };

      axios.post(url, body).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};
