export default {
  getPage(state) {
    const path = window.location.pathname.replace(/\//g, '').trim();
    return state.pages.find(it => it.path === path);
  },
  getAllPages(state) {
    return state.pages.filter(it => it.path !== '');
  },
  getLocation(state) {
    return state.currentLocation;
  },
  getItems(state) {
    let arr = [];
    state.categories.forEach(categ => {
      categ.items = state.filtered.filter(it => it.category == categ.id);
      arr.push(categ);
    });
    return arr;
  },
  getCategories(state) {
    return state.categories;
  },
  getOrderItems(state) {
    return state.ordered;
  },
  getAggrItems(state) {
    return [...state.aggregated.values()];
  },
  getOrderItemsCount(state) {
    let sum = 0;
    state.ordered.forEach(item => {
      sum += item.quantity;
    });
    return sum;
  },
  getOrderSum(state) {
    let sum = 0;
    state.ordered.forEach(item => {
      sum += item.orderPrice * item.quantity;
    });
    return sum;
  },
  getStreet(state) {
    return state.street;
  },
  getBuilding(state) {
    return state.building;
  },
  getEntrance(state) {
    return state.entrance;
  },
  getFloor(state) {
    return state.floor;
  },
  getRoom(state) {
    return state.room;
  },
  getTime(state) {
    return state.time;
  },
  getDeliveryType(state) {
    return state.deliveryType;
  },
  getLatLng(state) {
    return { lat: state.latitude, lng: state.longitude };
  },
  showDelivery(state) {
    return state.showDelivery;
  },
  getPolygons(state) {
    return state.polygons;
  },
  getSelectedPolygon(state) {
    return state.polygons.find(it => it.id === state.selectedPolygonId);
  },
  getInPolygonsStatus(state) {
    return state.inPolygons;
  },
  isMobile(state) {
    return state.isMobile;
  },
  isOrderSent(state) {
    return state.orderSent;
  },
  getPolygonWorktime(state) {
    return state.polygonWorktime;
  },
  getDeliveryType(state) {
    return state.deliveryType;
  },
  getThanksWindow(state) {
    return state.thanksWindow;
  },
  getPayParams(state) {
    return state.payParams;
  },
  getSelectedPayParam(state) {
    return state.payParams.find(it => it.id === state.selectedPayParam);
  },
  firstRoute(state) {
    return state.firstRoute;
  }
};
