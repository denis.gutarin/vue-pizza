import getDefault from './pagesDefault';
import pizzaNagatinskaya from './content/pizza-nagatinskaya';
import pizzaAnokhina from './content/pizza-anokhina';
import pizzaLeninskiyProspekt from './content/pizza-leninskiy';
import pizzaLobachevskogo from './content/pizza-lobachevskogo';
import pizzaMichurinskiy from './content/pizza-michurinskiy';
import pizzaNikulinskaya from './content/pizza-nikulinskaya';
import pizzaOlimpiyskayaDerevnya from './content/pizza-olimpiyskaya-derevnya';
import pizzaOzernaya from './content/pizza-ozernaya';
import pizzaPokryshkina from './content/pizza-pokryshkina';
import pizzaTroparevo from './content/pizza-troparevo';
import pizzaUdalcovo from './content/pizza-udalcovo';
import pizzaVernadskogo from './content/pizza-vernadskogo';
import pizzaYugoZapad from './content/pizza-yugo-zapad';
import pizzaYuzao from './content/pizza-yuzao';
const pages = [
  {
    path: '',
    place: '',
    polygonId: '0',
    content: pizzaNagatinskaya
  },
  {
    path: 'pizza-nagatinskaya',
    place: '',
    polygonId: '1',
    content: pizzaNagatinskaya
  },
  {
    path: 'pizza-anokhina',
    place: 'ул. Академика Анохина',
    polygonId: '2',
    content: pizzaAnokhina
  },
  {
    path: 'pizza-leninskiy',
    place: 'Ленинский проспект',
    polygonId: '2',
    content: pizzaLeninskiyProspekt
  },
  {
    path: 'pizza-lobachevskogo',
    place: 'ул. Лобачевского',
    polygonId: '2',
    content: pizzaLobachevskogo
  },
  {
    path: 'pizza-michurinskiy',
    place: 'Мичуринский проспект',
    polygonId: '2',
    content: pizzaMichurinskiy
  },
  {
    path: 'pizza-nikulinskaya',
    place: 'ул. Никулинская',
    polygonId: '2',
    content: pizzaNikulinskaya
  },
  {
    path: 'pizza-olimpiyskaya-derevnya',
    place: 'Олимпийская деревня',
    polygonId: '2',
    content: pizzaOlimpiyskayaDerevnya
  },
  {
    path: 'pizza-ozernaya',
    place: 'ул. Озерная',
    polygonId: '2',
    content: pizzaOzernaya
  },
  {
    path: 'pizza-pokryshkina',
    place: 'ул. Покрышкина',
    polygonId: '2',
    content: pizzaPokryshkina
  },
  {
    path: 'pizza-troparevo',
    place: 'Тропарево',
    polygonId: '2',
    content: pizzaTroparevo
  },
  {
    path: 'pizza-udalcovo',
    place: 'ул. Удальцово',
    polygonId: '2',
    content: pizzaUdalcovo
  },
  {
    path: 'pizza-vernadskogo',
    place: 'ул. Вернадского',
    polygonId: '2',
    content: pizzaVernadskogo
  },
  {
    path: 'pizza-yugo-zapad',
    place: 'Юго-Запад',
    polygonId: '2',
    content: pizzaYugoZapad
  },
  {
    path: 'pizza-yuzao',
    place: 'ЮЗАО',
    polygonId: '2',
    content: pizzaYuzao
  }
];

export default pages.map(it => ({
  ...it,
  title: getDefault.getTitle(it.place),
  description: getDefault.getDescription(it.place),
  firstHeader: getDefault.getFirstHeader(it.place),
  contentHeader: getDefault.getContentHeader(it.place),
  content: it.content.replace(/\n/g, '<br />')
}));
