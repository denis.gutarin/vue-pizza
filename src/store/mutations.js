import Vue from 'vue';
import { stringify } from 'querystring';
import _ from 'lodash';

function validate(state) {
  state.filtered.forEach(it => {
    const quan = state.ordered
      .filter(ord => ord.id === it.id)
      .reduce((sum, it) => {
        return sum + it.quantity;
      }, 0);
    Vue.set(it, 'quantity', quan);
    const s25 = it.sizes.find(it => it.selected == true);
    if (s25) {
      if (s25.id === 25) {
        const thin = it.thickness.find(it => it.id === 'thin');
        if (thin) Vue.set(thin, 'selected', false);
        const thick = it.thickness.find(it => it.id === 'thick');
        if (thick) Vue.set(thick, 'selected', true);
      }
    }
    const foundSize = it.sizes.find(el => el.selected == true);
    if (foundSize) {
      Vue.set(it, 'price', foundSize.price);
      Vue.set(it, 'orderPrice', foundSize.price);
    } else {
      Vue.set(it, 'orderPrice', it.price);
    }
  });

  // Акция

  const pizzaQuan = state.ordered.reduce((sum, it) => {
    return it.category === 'common-pizza' ? sum + it.quantity : sum;
  }, 0);

  state.ordered.forEach(a => (a.orderPrice = a.price));

  state.ordered.sort((a, b) => {
    if (a.category !== 'common-pizza') return 999999 + a.price - b.price;
    if (b.category !== 'common-pizza') return -999999 - a.price + b.price;
    return a.price - b.price;
  });

  let presentQuan = parseInt(pizzaQuan / 3);

  let index = 0;
  while (presentQuan > 0) {
    state.ordered[index].orderPrice = 0;
    index++;
    presentQuan--;
  }

  aggregateOrdered(state);
}

function aggregateOrdered(state) {
  state.aggregated = new Map();
  state.ordered.forEach(item => {
    const size = item.sizes.find(it => it.selected == true);
    const sizeText = size ? size.text : '';
    const thickness = item.thickness.find(it => it.selected == true);
    const thicknessText = thickness ? thickness.text : '';
    const sauce = item.sauces.find(it => it.selected == true);
    const sauceText = sauce ? sauce.text : '';
    const aggrKey =
      item.orderPrice > 0 ? item.aggrKey : item.aggrKey + ':bonus';

    const exist = state.aggregated.get(aggrKey);
    if (exist) {
      exist.quantity += 1;
    } else {
      const newItem = {
        aggrKey: item.orderPrice > 0 ? item.aggrKey : item.aggrKey + ':bonus',
        orderPrice: item.orderPrice,
        id: item.id,
        name: item.name,
        category: item.category,
        sizeText,
        thicknessText,
        sauceText,
        quantity: 1,
        isAggr: true
      };

      state.aggregated.set(aggrKey, newItem);
    }
  });
}

export { validate };

function formValidation(state) {
  state.validToOrder = true;
  state.validFio = true;
  state.validPhone = true;
  state.validStreet = true;
  state.validBuilding = true;
  state.validTime = true;
  if (!state.fio) state.validFio = false;
  if (!state.phone) state.validPhone = false;
  if (!state.street && state.deliveryType !== 2) state.validStreet = false;
  if (!state.building && state.deliveryType !== 2) state.validBuilding = false;
  if (!state.time && state.deliveryType !== 0) state.validTime = false;
  if (
    !state.validFio ||
    !state.validPhone ||
    !state.validStreet ||
    !state.validBuilding ||
    !state.validTime
  ) {
    state.validToOrder = false;
  }
}

export default {
  setLocation(state, payload) {
    console.log(payload);
    state.currentLocation = payload;
  },

  changeSizes(state, payload) {
    const item = state.getItem(payload.id);
    const filtered = state.getFilteredItem(payload.id);
    item.sizes = payload.values;
    filtered.sizes = payload.values;
    validate(state);
  },

  changeThickness(state, payload) {
    const item = state.getItem(payload.id);
    const filtered = state.getFilteredItem(payload.id);
    item.thickness = payload.values;
    filtered.thickness = payload.values;
    validate(state);
  },

  changeSauces(state, payload) {
    const item = state.getItem(payload.id);
    const filtered = state.getFilteredItem(payload.id);
    item.sauces = payload.values;
    filtered.sauces = payload.values;
    validate(state);
  },

  filterByCategory(state, categoryId) {
    if (!categoryId) {
      state.filtered = state.items;
      return;
    }
    state.filtered = state.items.filter(it => it.category === categoryId);
  },
  hideDelivery(state) {
    state.showDelivery = false;
  },
  addToOrder(state, item) {
    validate(state);

    if (item.isAggr) {
      const source = state.ordered.find(it => it.aggrKey === item.aggrKey);
      const newItem = JSON.parse(JSON.stringify(source));
      Vue.set(newItem, 'quantity', 1);
      Vue.set(newItem, 'orderKey', state.ordered.length + 1);
      Vue.set(newItem, 'aggrKey', item.aggrKey);
      state.ordered.push(newItem);
    } else {
      const source = item;
      const newItem = JSON.parse(JSON.stringify(source));
      Vue.set(newItem, 'quantity', 1);
      Vue.set(newItem, 'orderKey', state.ordered.length + 1);
      Vue.set(newItem, 'aggrKey', state.getAggrKey(newItem));
      state.ordered.push(newItem);
    }

    validate(state);
  },

  delFromOrderByProduct(state, id) {
    let orderedIndex = state.ordered
      .slice()
      .reverse()
      .findIndex(it => it.id === id);
    if (typeof orderedIndex == 'undefined') return;
    orderedIndex = state.ordered.length - 1 - orderedIndex;
    const ordered = state.ordered[orderedIndex];
    if (ordered.quantity > 1) {
      ordered.quantity -= 1;
    } else {
      state.ordered.splice(orderedIndex, 1);
    }
    validate(state);
  },

  delAllFromOrderByKey(state, aggrKey) {
    state.ordered = state.ordered.filter(it => it.aggrKey !== aggrKey);
    validate(state);
  },
  delFromOrderByKey(state, aggrKey) {
    let orderedIndex = state.ordered
      .slice()
      .reverse()
      .findIndex(it => it.aggrKey === aggrKey);
    if (typeof orderedIndex == 'undefined') return;
    orderedIndex = state.ordered.length - 1 - orderedIndex;
    const ordered = state.ordered[orderedIndex];
    if (ordered.quantity > 1) {
      ordered.quantity -= 1;
    } else {
      state.ordered.splice(orderedIndex, 1);
    }
    validate(state);
  },
  updateLatLng(state, value) {
    state.latitude = value.latitude;
    state.longitude = value.longitude;
  },
  updateFio(state, value) {
    state.fio = value;
    if (!state.validToOrder) formValidation(state);
  },
  updatePhone(state, value) {
    state.phone = value;
    if (!state.validToOrder) formValidation(state);
  },
  updateEmail(state, value) {
    state.email = value;
  },
  updateComment(state, value) {
    state.comment = value;
  },
  updateStreet(state, object) {
    state.map = object.map;
    state.street = object.value;
    if (!state.validToOrder) formValidation(state);
  },
  updateBuilding(state, object) {
    state.map = object.map;
    state.building = object.value;
    if (!state.validToOrder) formValidation(state);
  },
  updateEntrance(state, object) {
    state.entrance = object.value;
  },
  updateFloor(state, object) {
    state.floor = object.value;
  },
  updateRoom(state, object) {
    state.room = object.value;
  },
  updateTime(state, object) {
    state.time = object.value;
  },
  updateChargeFor(state, value) {
    state.chargeFor = value;
  },
  changeDeliveryType(state, value) {
    state.deliveryType = value;
  },
  changeDeliverySource(state, value) {
    state.selectedPolygonId = value;
  },
  changePayParam(state, value) {
    state.selectedPayParam = value;
  },
  formValidation(state) {
    return formValidation(state);
  },
  checkDelivery(state, point) {
    const map = state.map;
    state.inPolygons = 2;
    state.polygons.forEach(polygon => {
      console.log(polygon.text);
      const googlePolygon = new google.maps.Polygon({ paths: polygon.path });
      const googleLatLng = new google.maps.LatLng(
        point.latitude,
        point.longitude
      );
      console.log(JSON.stringify(polygon.path));
      console.log(JSON.stringify(point));
      const inPolygon = google.maps.geometry.poly.containsLocation(
        googleLatLng,
        googlePolygon
      );
      if (inPolygon) {
        console.log('In polygon');
        state.inPolygons = 1;
        state.polygonId = polygon.id;
        state.polygonText = polygon.text;
        state.polygonMail = polygon.mail;
        state.polygonWorktime = polygon.worktime;
      }
      return;
    });
  },
  setOrderSent(state, payload) {
    state.orderSent = true;
  },
  updateCallRequire(state) {
    state.callRequire = !state.callRequire;
  },
  reset(state) {
    state.orderSent = false;
    state.ordered = [];
    state.callRequire = false;
    validate(state);
  },
  showThanksWindow(state) {
    state.thanksWindow = true;
  },
  hideThanksWindow(state) {
    state.thanksWindow = false;
  },
  setFirstRoute(state, payload) {
    state.firstRoute = payload;
    if (payload == '/pizza-nagatinskaya/') {
      state.selectedPolygonId = '1';
    } else if (payload == '/pizza-anokhina/') {
      state.selectedPolygonId = '2';
    }
  },
  selectSauce(state, payload) {
    const item = state.ordered.find(it => (it.orderKey = payload.orderKey));
    if (!item) return;
    item.sauces.forEach(it => {
      if (it.id == payload.sauceId) {
        it.selected = true;
      } else {
        it.selected = false;
      }
    });
  }
};
