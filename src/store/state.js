import categoriesJson from '../assets/categories.json';
import itemsJson from '../assets/items.json';
import defPolygons from './polygons';
import pages from './pages.js';

export function initItems(state) {
  const items = itemsJson.items.map(it => {
    if (it.category.indexOf('pizza') >= 0) {
      it.sauces = [
        { id: 'none', text: 'без соуса', selected: true },
        { id: 'tomato', text: 'томатный', selected: false },
        { id: 'balsamiq', text: 'песто', selected: false },
        { id: 'garlick', text: 'чесночный', selected: false }
      ];
    } else {
      it.sauces = [];
    }
    it.id = `${it.category}-${it.image}`;
    return it;
  });
  state.items = items;
  state.filtered = items;
}

const params = new URLSearchParams(window.location.search);

export default {
  firstRoute: '/mapizza.ru/',
  currentLocation: 'catalog', //catalog, order, contacts
  utm_source: params.get('utm_source'),
  utm_medium: params.get('utm_medium'),
  utm_campaign: params.get('utm_campaign'),
  utm_content: params.get('utm_content'),
  utm_term: params.get('utm_term'),
  yclid: params.get('yclid'),
  isMobile: /Android|webOS|iPhone|iPod|Opera Mini/i.test(navigator.userAgent),
  showDelivery: false,
  thanksWindow: false,
  validToOrder: true,
  orderSent: false,
  fio: '',
  validFio: true,
  email: '',
  comment: '',
  deliveryType: 0, //0 - доставка сейчас, 1 - доставка ко времени, 2 - самовывоз
  time: '',
  validTime: true,
  phone: '+7(',
  validPhone: true,
  street: '',
  validStreet: true,
  building: '',
  validBuilding: true,
  entrance: '',
  floor: '',
  room: '',
  callRequire: false,
  latitude: 0.0,
  longitude: 0.0,
  polygons: defPolygons,
  polygonId: '1',
  polygonText: 'Нагатинская',
  polygonMail: 'Motsnapizza@gmail.com',
  polygonWorktime: '11:00 - 23:00',
  selectedPolygonId: '1',
  inPolygons: 0, //0 - не задан, 1 - входит в зоу, 2 не входит в зону
  pages: pages,
  payParams: [
    { id: 'cash', text: 'Оплата наличными' },
    { id: 'card', text: 'Оплата картой' }
  ],
  selectedPayParam: 'cash',
  chargeFor: '',
  map: {},
  addrSearch: '',
  items: [],
  filtered: [],
  categories: categoriesJson.categories,
  ordered: [],
  aggregated: new Map(),

  getAggrKey(item) {
    const id = item.id;
    const size = item.sizes.find(it => it.selected == true);
    const sizeId = size ? size.id : 0;
    const thickness = item.thickness.find(it => it.selected == true);
    const thicknessId = thickness ? thickness.id : 0;
    const sauce = item.sauces.find(it => it.selected == true);
    const sauceId = sauce ? sauce.id : 0;
    return `${id}:${sizeId}:${thicknessId}:${sauceId}`;
  },
  getItem(id) {
    return this.items.find(it => it.id === id);
  },
  getFilteredItem(id) {
    return this.filtered.find(it => it.id === id);
  }
};
