import 'babel-polyfill';
import 'url-search-params-polyfill';
import Vue from 'vue';
import Vuex from 'vuex';
import Router from 'vue-router';
import * as VueGoogleMaps from 'vue2-google-maps';
import vueScrollTo from 'vue-scrollto';
import createPersistedState from 'vuex-persistedstate';

import state, { initItems } from './store/state';
import actions from './store/actions';
import mutations from './store/mutations';
import { validate } from './store/mutations';
import getters from './store/getters';
import { GAPIKEY } from './store/actions';

import App from './App.vue';

// const routes = [
//   { path: '/', component: Catalog },
//   { path: '/main', component: Catalog },
//   { path: '/pizza-nagatinskaya', component: Catalog },
//   { path: '/pizza-anokhina', component: Catalog },
//   { path: '/order', component: Order },
//   { path: '/contacts', component: Contacts }
// ];
//
// state.categories.forEach(element => {
//   routes.push({
//     path: '/catalog' + element.url,
//     component: Catalog
//   });
// });
//
// Vue.use(Router);
// const router = new Router({
//   mode: 'history',
//   routes
// });

Vue.use(Vuex);
const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [
    createPersistedState({
      paths: [
        'ordered',
        'fio',
        'phone',
        'comment',
        'street',
        'building',
        'entrance',
        'floor',
        'room',
        'time'
      ]
    })
  ]
});

initItems(store.state);
validate(store.state);

Vue.use(VueGoogleMaps, {
  installComponents: true,
  load: {
    key: GAPIKEY,
    libraries: 'places'
  }
});

Vue.use(vueScrollTo, {
  duration: 330
});

Vue.filter('uppercase', function(value) {
  if (!value) return '';
  value = value.toString();
  return value.toUpperCase();
});

Vue.filter('lowercase', function(value) {
  if (!value) return '';
  value = value.toString();
  return value.toLowerCase();
});

// store.dispatch('sendMail');

new Vue({
  el: '#appmain',
  store,
  // router,
  render: h => h(App),
  mounted() {
    document.dispatchEvent(new Event('render-event'));
  }
});
