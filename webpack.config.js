var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var PrerenderSPAPlugin = require('prerender-spa-plugin');
var Renderer = PrerenderSPAPlugin.PuppeteerRenderer;

module.exports = {
  entry: ['babel-polyfill', './src/main.js'],
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'build.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {}
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    // new HtmlWebpackPlugin({
    //   title: 'PRODUCTION prerender-spa-plugin',
    //   template: 'template.html',
    //   filename: path.resolve(__dirname, 'index.html')
    // }),
    new PrerenderSPAPlugin({
      // staticDir: path.join(__dirname, 'dist'),
      staticDir: __dirname,
      outputDir: path.join(__dirname, 'prerendered'),
      routes: [
        '/',
        '/pizza-anokhina',
        '/pizza-leninskiy',
        '/pizza-lobachevskogo',
        '/pizza-michurinskiy',
        '/pizza-nagatinskaya',
        '/pizza-nikulinskaya',
        '/pizza-olimpiyskaya-derevnya',
        '/pizza-ozernaya',
        '/pizza-porkyshkina',
        '/pizza-troparevo',
        '/pizza-udalcovo',
        '/pizza-vernadskogo',
        '/pizza-yugo-zapad',
        '/pizza-yuzao'
      ],
      renderer: new Renderer({
        headless: true,
        renderAfterDocumentEvent: 'render-event'
      })
    })
  ]);
}
